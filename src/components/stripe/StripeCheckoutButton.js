import React from 'react'

import StripeCheckout from 'react-stripe-checkout';

const StripeCheckoutButton = ({ price }) => {
    const PriceForStripe = price * 100;
    const publishableKey = 'pk_test_XYN7qSCLZ1S6PSBGKRBcVfKu00Hr1Nnf8W';

    const onToken = token => {
        console.log(token);
    }
    return (
        <StripeCheckout
        label='Pay Now'
        name='CRWN CLOTHING PVT LTD'
        billingAddress
        shippingAddress
        image='https://svgshare.com/i/CUz.svg'
        description={`Yout total is  $${price}`}
        amount={PriceForStripe}
        panelLabel='Pay Now'
        token={onToken}
        stripeKey={publishableKey}
         />
    )
}
export default StripeCheckoutButton;