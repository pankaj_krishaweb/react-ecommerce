import React from 'react'
import './home.css'
import Directory from '../directory/Directory'

function homePage({history, match}) {
    return (
        <div className='homepage'>
            <Directory></Directory>
        </div>
    )
}

export default homePage
