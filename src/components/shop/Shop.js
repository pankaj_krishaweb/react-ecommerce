import React from 'react'
import CollectionOverview from '../collection-overview/CollectionOverview'
import {Route} from 'react-router-dom'
import Collection from '../pages/collection/Collection'

const ShopPage = ({ match }) => (
    <div className='shop-page'>
      <Route exact path={`${match.path}`} component={CollectionOverview} />
      <Route path={`${match.path}/:collectionId`} component={Collection} />
    </div>
  );

export default ShopPage
