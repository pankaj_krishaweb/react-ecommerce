import React from 'react'
import Menu from '../menu-item/Menu'
import './directory.css';
import { createStructuredSelector } from 'reselect'
import { connect } from 'react-redux';
import { selectDirectorySections } from '../../redux/directory/directory.selector';

const  Directory = ({ sections }) => (
    <div className='directory-menu'>
        {
            sections.map(({ id, ...otherSectionProps }) => (
                <Menu key={id} {...otherSectionProps}></Menu>
            ))
        }
    </div>
)
const mapStateToProps = createStructuredSelector({
    sections: selectDirectorySections
});
export default connect(mapStateToProps)(Directory)
