import React from 'react'
import { createStructuredSelector } from 'reselect'
import './CollectionOverview.scss'
import { connect } from 'react-redux'
import PreviewCollection from '../preview-collection/PreviewCollection'
import { selectCollectionsForPreview } from '../../redux/shop/shop.selector'

const CollectionOverview = ({ collections }) =>(
    <div className='collections-overview'>
        {
            collections.map(({id,...otherCollectionProps}) => (
                    <PreviewCollection key={id} {...otherCollectionProps}></PreviewCollection>
                ))
        }
    </div>
)

const mapStateToProps = createStructuredSelector({
    collections : selectCollectionsForPreview
})
export default connect(mapStateToProps)(CollectionOverview)