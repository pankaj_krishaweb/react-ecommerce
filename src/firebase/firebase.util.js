import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyCVfQBbRGCRJus9DG7NIR_nTr_UunTkd-I",
    authDomain: "first-react-app-51cfb.firebaseapp.com",
    databaseURL: "https://first-react-app-51cfb.firebaseio.com",
    projectId: "first-react-app-51cfb",
    storageBucket: "first-react-app-51cfb.appspot.com",
    messagingSenderId: "94006832811",
    appId: "1:94006832811:web:67ae6205ea47cd5dde91ee",
    measurementId: "G-7JM74KNYY9"
  };
  firebase.initializeApp(config);

  export const createUserProfileDocument = async(userAuth,additionalData)=>{
    if (!userAuth) return;
    const userRef =  firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get();
    if(!snapShot.exists){
      const {displayName, email} = userAuth;
      const createdAt = new Date();
      try {
        await userRef.set({
          displayName,
          email,
          createdAt,
          ...additionalData
        })
      } catch (error) {
        console.log('error creating user', error.message);
      }
    }
    return userRef;
    
  }

  export const auth = firebase.auth();
  export const firestore = firebase.firestore();

  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({prompt : 'select_account'});
  export const signInWithGoogle = () => auth.signInWithPopup(provider);

  export default firebase;